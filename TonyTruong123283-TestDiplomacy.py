#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

# FIX ->>>>>>
class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Hold\nE Austin Support A"
        l = diplomacy_read(s)
        self.assertEqual(l,  ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Hold", "E Austin Support A"])
        
    def test_read2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid"
        l = diplomacy_read(s)
        self.assertEqual(l,  ["A Madrid Hold", "B Barcelona Move Madrid"])
        
    def test_read3(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support C"
        l = diplomacy_read(s)
        self.assertEqual(l,  ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Support C"])

    # ----
    # eval
    # ----


    def test_eval_1(self):

        v =  diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Hold", "E Austin Support A"])
        self.assertEqual(v, 'A Madrid\nB [dead]\nC [dead]\nD Paris\nE Austin')

        v1 = diplomacy_eval(["A Madrid Hold","B Barcelona Move Madrid", "C London Support B", "D Austin Move London"])
        self.assertEqual(v1, 'A [dead]\nB [dead]\nC [dead]\nD [dead]')
	 
        #invalid input A Hold London is wrong
        v2 = diplomacy_eval([])
        self.assertEqual(v2, 'Error')
     
        v3 = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B"])
        self.assertEqual(v3, 'A [dead]\nB Madrid\nC [dead]\nD Paris')

    def test_eval_2(self):
     
        v = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid"])
        self.assertEqual(v, 'A [dead]\nB [dead]')
	 
        v1 = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"])	
        self.assertEqual(v1, 'A [dead]\nB Madrid\nC London')
	
        #invalid input because there is only 1 city
        v2 = diplomacy_eval(["A Madrid Move Paris", "B Paris Move Madrid"])
        self.assertEqual(v2, 'A Paris\nB Madrid')

        v3 = diplomacy_eval(["A Madrid Move Barcelona", "B Barcelona Move Madrid"])
        self.assertEqual(v3, 'A Barcelona\nB Madrid')

    def test_eval_3(self): 
        v = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Support C"])
        self.assertEqual(v, "A [dead]\nB Madrid\nC London\nD Austin")
        
        #error
        v1 = diplomacy_eval(["A Madrid Move Barcelona", "B Barcelona Move Madrid"])
        self.assertEqual(v1, "A Barcelona\nB Madrid")

        v2 = diplomacy_eval(["A Madrid Move Austin", "B Barcelona Hold", "C London Support B", "D Austin Support C"])
        self.assertEqual(v2, "A [dead]\nB Barcelona\nC London\nD [dead]")

        v3 = diplomacy_eval(["A Madrid Move Barcelona", "B Barcelona Move Madrid", "C London Move Austin", "D Austin Move London"])
        self.assertEqual(v3, "A Barcelona\nB Madrid\nC Austin\nD London")

    def test_eval_4(self):
        v = diplomacy_eval(["A Austin Hold", "C Madrid Hold"])
        self.assertEqual(v, "A Austin\nC Madrid")

        v1 = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C Chicago Move Barcelona"])
        self.assertEqual(v1, "A [dead]\nB [dead]\nC Barcelona")

        v2 = diplomacy_eval(["A Madrid Move Barcelona", "B Barcelona Support C", "C London Move Madrid"])
        self.assertEqual(v2, "A [dead]\nB [dead]\nC Madrid")

        v3 = diplomacy_eval(["A Madrid Support B", "B Barcelona Support C", "C London Support D", "D Austin Move Madrid"])
        self.assertEqual(v3, "A [dead]\nB Barcelona\nC London\nD Madrid")
    
    # -----
    # solve
    # -----
    
    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Hold\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
        
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n") 
    
    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD Austin\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()
