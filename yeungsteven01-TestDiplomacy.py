#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_print, diplomacy_fight, diplomacy_supportWho, diplomacy_supportCount, diplomacy_endLoc

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = {'Hold': ['B', 'Barcelona'], 'Support': ['C', 'Austin', 'A', 'T', 'Miami', 'B', 'F', 'Seattle', 'B'], 'Move': ['A', 'Madrid', 'Barcelona']}
        w = diplomacy_solve(r)
        self.assertEqual(
            w, {'A': '[dead]', 'B': 'Barcelona', 'C': 'Austin', 'F': 'Seattle', 'T': 'Miami'})

    def test_solve_2(self):
        r = {'Hold': ['A', 'Madrid'], 'Support': ['D', 'Paris', 'B'], 'Move': ['B', 'Barcelona', 'Madrid', 'C', 'London', 'Madrid']}
        w = diplomacy_solve(r)
        self.assertEqual(
            w, {'A': '[dead]', 'B': 'Madrid', 'C': '[dead]', 'D': 'Paris'})

    def test_solve_3(self):
        r = {'Hold': ['Z', 'Austin','A','Houston'], 'Support': ['D', 'Dallas', 'Z'], 'Move': ['R', 'Boston', 'Austin']}
        w = diplomacy_solve(r)
        self.assertEqual(
            w, {'A': 'Houston', 'D': 'Dallas', 'R': '[dead]', 'Z': 'Austin'})
    
    def test_solve_4(self):
        r = {'Hold': ['A', 'Madrid'], 'Support': ['D', 'Paris', 'B'], 'Move': ['B', 'Barcelona', 'Madrid', 'C', 'London', 'Paris']}
        w = diplomacy_solve(r)
        self.assertEqual(
            w, {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': '[dead]'})
    # -----
    # read
    # -----

    def test_print_1(self):
        r = 'A Madrid Move Barcelona\nB Barcelona Hold\nC Austin Support A\nT Miami Support B\nF Seattle Support B'
        w = diplomacy_read(r)
        self.assertEqual(
            w, {'Hold': ['B', 'Barcelona'], 'Support': ['C', 'Austin', 'A', 'T', 'Miami', 'B', 'F', 'Seattle', 'B'], 'Move': ['A', 'Madrid', 'Barcelona']})

    def test_print_2(self):
        r = 'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B'
        w = diplomacy_read(r)
        self.assertEqual(
            w, {'Hold': ['A', 'Madrid'], 'Support': ['D', 'Paris', 'B'], 'Move': ['B', 'Barcelona', 'Madrid', 'C', 'London', 'Madrid']})

    def test_print_3(self):
        r = 'Z Austin Hold\nA Houston Hold\nD Dallas Support Z\nR Boston Move Austin'
        w = diplomacy_read(r)
        self.assertEqual(
            w, {'Hold': ['Z', 'Austin','A','Houston'], 'Support': ['D', 'Dallas', 'Z'], 'Move': ['R', 'Boston', 'Austin']})
# ----
# main
# ----


if __name__ == "__main__":  #pragma : no cover
    main()