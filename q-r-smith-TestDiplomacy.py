# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_solve, diplomacy_read

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # eval
    # ----
    def test_eval_1(self):
        v = diplomacy_eval(["A London Hold", "B Austin Support A", "C Madrid Move London"])
        self.assertEqual(v, [["A", "London"],["B", "Austin"], ["C", "[dead]"]])
    def test_eval_2(self):
        v = diplomacy_eval(["A London Hold", "B Austin Support A", "C Madrid Move London", "D Dallas Support C"])
        self.assertEqual(v, [["A", "[dead]"], ["B", "Austin"], ["C", "[dead]"], ["D", "Dallas"]])
    def test_eval_3(self):
        v = diplomacy_eval(["A London Hold", "B Austin Support A", "C Madrid Move London", "D Dallas Support C", "E Legoland Support C"])
        self.assertEqual(v,[["A", "[dead]"], ["B", "Austin"], ["C", "London"], ["D", "Dallas"], ["E", "Legoland"]])
    def test_eval_4(self):
        v = diplomacy_eval(["A Legoland Hold", "B Paris Hold", "C Madrid Move Paris"])
        self.assertEqual(v,[["A", "Legoland"], ["B", "[dead]"], ["C", "[dead]"]])
    def test_eval_5(self):
        v = diplomacy_eval(["A Dallas Move Houston", "B Houston Move Austin", "C Austin Move Dallas"])
        self.assertEqual(v, [["A", "Houston"],["B", "Austin"], ["C", "Dallas"]])
    def test_eval_6(self):
        v = diplomacy_eval(["A London Hold", "B Austin Support A", "C Madrid Move London", "D Dallas Move Austin"])
        self.assertEqual(v,[["A", "[dead]"], ["B", "[dead]"], ["C", "[dead]"], ["D", "[dead]"]])


    # ----
    # diplomacy_solve unit tests
    # ----

    def test_solve_1(self):
        r = StringIO("A Austin Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\n")

    def test_solve_2(self):
        r = StringIO("A Legoland Move Houston\nB NewYork Support A\nC Houston Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB NewYork\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Dallas Move Austin\nB LosAngeles Support A\nC Houston Support A\nD Austin Move Houston\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB LosAngeles\nC [dead]\nD [dead]\n")
# ----
# main
# ----


if __name__ == "__main__": #pragma no cover
    main()