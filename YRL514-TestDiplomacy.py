from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_print

class TestDiplomacy(TestCase):
    def test_diplomacy_solve1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A Madrid\n')
    def test_diplomacy_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')
    def test_diplomacy_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC [dead]\nD Paris\n')
        
        
if __name__ == "__main__": #pragma: no cover
    main()
