"""Unit tests for diplomacy_solve """

# -------
# imports
# -------

from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve, diplomacy_output
from Diplomacy import WarAction
from io import StringIO

# -------------
# TestDiplomacy
# -------------

# Write unit tests in TestDiplomacy.py that test corner cases and failure cases until you have an 3 tests for 
# the function diplomacy_solve(), confirm the expected failures, and add, commit, and push to the private code repo

class TestDiplomacy(TestCase):

    # ----
    # read
    # ----

    def test_read_1(self):
        f = diplomacy_read('RunDiplomacy1.in')
        self.assertEqual(f, [['A', 'Nashville', 'Hold'], ['B', 'Miami', 'Move', 'Nashville'], ['C', 'Raleigh', 'Move', 'Nashville'], ['D', 'Atlanta', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']])


    def test_read_2(self):
        f = diplomacy_read('RunDiplomacy2.in')
        self.assertEqual(f, [['A', 'LosAngeles', 'Hold'], ['B', 'Dallas', 'Move', 'LosAngeles'], ['C', 'LasVegas', 'Support', 'B'], ['D', 'Austin', 'Move', 'LasVegas']])


    def test_read_3(self):
        f = diplomacy_read('RunDiplomacy3.in')
        self.assertEqual(f, [['A', 'LasVegas', 'Hold'], ['B', 'Miami', 'Move', 'LasVegas'], ['C', 'Minneapolis', 'Move', 'LasVegas']])


    # ----
    # eval
    # ----

    def test_diplomacy_eval_1(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']])
        correct = [WarAction('A', 'Madrid', 'Hold'), WarAction('B', 'Barcelona', 'Move', 'Madrid'), WarAction('C', 'London', 'Support', 'B')]
        for i in range(len(v)):
            self.assertEqual(v[i].army, correct[i].army)
            self.assertEqual(v[i].location, correct[i].location)
            self.assertEqual(v[i].action, correct[i].action)
            self.assertEqual(v[i].other, correct[i].other)


    def test_diplomacy_eval_2(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']])
        correct = [WarAction('A', 'Madrid', 'Hold'), WarAction('B', 'Barcelona', 'Move', 'Madrid'), WarAction('C', 'London', 'Move', 'Madrid'), WarAction('D', 'Paris', 'Support', 'B'), WarAction('E', 'Austin', 'Support', 'A')]
        for i in range(len(v)):
            self.assertEqual(v[i].army, correct[i].army)
            self.assertEqual(v[i].location, correct[i].location)
            self.assertEqual(v[i].action, correct[i].action)
            self.assertEqual(v[i].other, correct[i].other)


    def test_diplomacy_eval_3(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid']])
        correct = [WarAction('A', 'Madrid', 'Hold'), WarAction('B', 'Barcelona', 'Move', 'Madrid'), WarAction('C', 'London', 'Move', 'Madrid')]
        for i in range(len(v)):
            self.assertEqual(v[i].army, correct[i].army)
            self.assertEqual(v[i].location, correct[i].location)
            self.assertEqual(v[i].action, correct[i].action)
            self.assertEqual(v[i].other, correct[i].other)



    # -----
    # solve
    # -----

    def test_diplomacy_solve_1(self):
        v = diplomacy_solve(diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]))
        status = []
        cities = []
        for item in v:
            status.append(item.alive)
            if item.alive:
                cities.append(item.location)
        self.assertEqual(status, [False, True, True])
        self.assertEqual(cities, ['Madrid', 'London'])



    def test_diplomacy_solve_2(self):
        v = diplomacy_solve(diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']]))
        status = []
        cities = []
        for item in v:
            status.append(item.alive)
            if item.alive:
                cities.append(item.location)
        self.assertEqual(status, [False, False, False, True, True])
        self.assertEqual(cities, ['Paris', 'Austin'])


    def test_diplomacy_solve_3(self):
        v = diplomacy_solve(diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid']]))
        status = []
        cities = []
        for item in v:
            status.append(item.alive)
            if item.alive:
                cities.append(item.location)
        self.assertEqual(status, [False, False, False])
        self.assertEqual(cities, [])

    def test_diplomacy_solve_4(self):
        v = diplomacy_solve(diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], ['D', 'Austin', 'Move', 'London']]))
        status = []
        cities = []
        for item in v:
            status.append(item.alive)
            if item.alive:
                cities.append(item.location)
        self.assertEqual(status, [False, False, False, False])
        self.assertEqual(cities, [])

    def test_diplomacy_solve_5(self):
        v = diplomacy_solve(diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B']]))
        status = []
        cities = []
        for item in v:
            status.append(item.alive)
            if item.alive:
                cities.append(item.location)
        self.assertEqual(status, [False, True, False, True])
        self.assertEqual(cities, ['Madrid', 'Paris'])

    def test_diplomacy_solve_6(self):
        v = diplomacy_solve(diplomacy_eval([['A', 'Austin', 'Move', 'Houston'], ['B', 'Houston', 'Move', 'Austin']]))
        status = []
        cities = []
        for item in v:
            status.append(item.alive)
            if item.alive:
                cities.append(item.location)
        self.assertEqual(status, [True, True])
        self.assertEqual(cities, ['Houston', 'Austin'])

    
    # ------
    # output
    # ------
    def test_diplomacy_output_1(self):
        v = diplomacy_solve(diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]))
        w = StringIO()
        diplomacy_output(v, w)
        self.assertEqual(w.getvalue(),"A [dead]\nB Madrid\nC London\n")



    def test_diplomacy_output_2(self):
        v = diplomacy_solve(diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']]))
        w = StringIO()
        diplomacy_output(v, w)
        self.assertEqual(w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


    def test_diplomacy_output_3(self):
        v = diplomacy_solve(diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid']]))
        w = StringIO()
        diplomacy_output(v, w)
        self.assertEqual(w.getvalue(),"A [dead]\nB [dead]\nC [dead]\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()


""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
"""
